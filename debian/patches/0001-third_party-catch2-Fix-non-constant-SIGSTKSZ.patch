From 830dbd151a5743629f3c6fb2827bb65bb6bca70d Mon Sep 17 00:00:00 2001
From: Alexandre Ghiti <alexandre.ghiti@canonical.com>
Date: Tue, 3 May 2022 15:15:12 +0200
Subject: [PATCH] third_party: catch2: Fix non-constant SIGSTKSZ

SIGSTKSZ is not constant anymore so altStackMem must be allocated
dynamically: this patch implements what is done upstream.

Signed-off-by: Alexandre Ghiti <alexandre.ghiti@canonical.com>
---
 third_party/catch2/include/catch2/catch.hpp | 23 ++++++++++++++++++---
 1 file changed, 20 insertions(+), 3 deletions(-)

diff --git a/third_party/catch2/include/catch2/catch.hpp b/third_party/catch2/include/catch2/catch.hpp
index 081cb41..2de9330 100644
--- a/third_party/catch2/include/catch2/catch.hpp
+++ b/third_party/catch2/include/catch2/catch.hpp
@@ -4731,7 +4731,8 @@ namespace Catch {
         static bool isSet;
         static struct sigaction oldSigActions[];
         static stack_t oldSigStack;
-        static char altStackMem[];
+        static char* altStackMem;
+        static std::size_t altStackSize;
 
         static void handleSignal( int sig );
 
@@ -7226,6 +7227,11 @@ namespace {
     void reportFatal( char const * const message ) {
         Catch::getCurrentContext().getResultCapture()->handleFatalErrorCondition( message );
     }
+
+    //! Minimal size Catch2 needs for its own fatal error handling.
+    //! Picked empirically, so it might not be sufficient on all
+    //! platforms, and for all configurations.
+    constexpr std::size_t minStackSizeForErrors = 32 * 1024;
 }
 
 #endif // signals/SEH handling
@@ -7318,10 +7324,16 @@ namespace Catch {
     }
 
     FatalConditionHandler::FatalConditionHandler() {
+        assert(!altStackMem && "Cannot initialize POSIX signal handler when one already exists");
+        if (altStackSize == 0) {
+            altStackSize = std::max(static_cast<size_t>(SIGSTKSZ), minStackSizeForErrors);
+        }
+        altStackMem = new char[altStackSize]();
+
         isSet = true;
         stack_t sigStack;
         sigStack.ss_sp = altStackMem;
-        sigStack.ss_size = SIGSTKSZ;
+        sigStack.ss_size = altStackSize;
         sigStack.ss_flags = 0;
         sigaltstack(&sigStack, &oldSigStack);
         struct sigaction sa = { };
@@ -7334,6 +7346,10 @@ namespace Catch {
     }
 
     FatalConditionHandler::~FatalConditionHandler() {
+        delete[] altStackMem;
+        // We signal that another instance can be constructed by zeroing
+        // out the pointer.
+        altStackMem = nullptr;
         reset();
     }
 
@@ -7352,7 +7368,8 @@ namespace Catch {
     bool FatalConditionHandler::isSet = false;
     struct sigaction FatalConditionHandler::oldSigActions[sizeof(signalDefs)/sizeof(SignalDefs)] = {};
     stack_t FatalConditionHandler::oldSigStack = {};
-    char FatalConditionHandler::altStackMem[SIGSTKSZ] = {};
+    char* FatalConditionHandler::altStackMem = nullptr;
+    std::size_t FatalConditionHandler::altStackSize = 0;
 
 } // namespace Catch
 
-- 
2.34.1


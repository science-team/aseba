Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: aseba
Source: https://github.com/aseba-community/aseba/archive/1.5.5.zip

Files: *
Comment: more detailed copyright information was available in year 2018
 at https://github.com/aseba-community/aseba/blob/master/authors.txt;
 this file is appended to /usr/share/doc/aseba as a gzipped archive.
Copyright: 2007-2018 Stephane Magnenat <stephane at magnenat dot net> (http://stephane.magnenat.net original idea, architecture, core coder)
           2007-2015 Florian Vaussard <florian dot vaussard at epfl dot ch> (core coder, Windows maintainer)
	   2007-2018 Philippe Retornaz <philippe dot retornaz at epfl dot ch> (coder, embedded integration)
	   2012 Jiwon Shin <jiwon dot shin at mavt dot ethz dot ch> (coder, VPL)
	   2016-2018 David James Sherman <david dot sherman at inria dot fr> (asebahttp, Scratch integration)
	   2016 Fabian Hahn <fabian at hahn dot graphics> (Blockly integration)
	   2016 Maria María Beltrán <maria dot beltranreyes at epfl dot ch> (VPL design - funded by Gebert Rüf Stiftung)
	   2016 Manon Briod <manon dot briod at epfl dot ch> (VPL design - funded by Gebert Rüf Stiftung)
	   2018 Sandra Moser (German translation)
	   2018 Francisco Javier Botero Herrera <fboteroh at gmail dot com> (Spanish translation)
	   2018 Ezio Somá <ezio dot soma at gmail dot com> (Italian translation)
	   2018 Michael Bonani <michael dot bonani at mobsya dot org> (French translation)
	   2018 Fanny Riedo <fanny dot riedo at mobsya dot org> (OS X maintainer)
	   2018 Valentin Longchamp <valentin dot longchamp at epfl dot ch> (inputs on framework architecture, additional coding)
	   2018 Basilio Noris <basilio dot noris at epfl dot ch> (inputs on challenge, graphics design on challenge)
License: LGPL-3

Files: debian/*
Copyright: 2018 Georges Khaznadar <georgesk@debian.org>
License: GPL-3.0+

License: LGPL-3
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU Lesser General
 Public License version 3 can be found in "/usr/share/common-licenses/LGPL-3".


License: GPL-3.0+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General
 Public License version 3 can be found in "/usr/share/common-licenses/GPL-3".
